package main

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var (
		secrets []Secret
		vulns   []report.Vulnerability
	)

	err := json.NewDecoder(reader).Decode(&secrets)
	if err != nil {
		log.Errorf("Couldn't parse the Gitleaks report: %v\n", err)
		return nil, err
	}

	for _, secret := range secrets {
		if secret.Commit == "" {
			// update file to use relative path if scan is from a `no-git`
			// gitleaks report
			cwd, err := os.Getwd()
			if err != nil {
				log.Errorf("Could not get working dir: %v\n", err)
			} else {
				relPath, err := filepath.Rel(cwd, secret.File)
				if err != nil {
					log.Errorf("Could not generate relative path to working dir: %v\n", err)
				} else {
					secret.File = relPath
				}
			}
		}

		vulns = append(vulns, report.Vulnerability{
			Category:             metadata.Type,
			Scanner:              metadata.IssueScanner,
			Name:                 secret.Description,
			Message:              secret.message(),
			Description:          secret.description(),
			CompareKey:           secret.compareKey(),
			Severity:             report.SeverityLevelCritical,
			Confidence:           report.ConfidenceLevelUnknown,
			Location:             secret.location(),
			Identifiers:          secret.identifiers(),
			RawSourceCodeExtract: truncate(secret.Secret, maxSourceCodeLen),
		})
	}

	newReport := report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSecretDetection
	newReport.Vulnerabilities = vulns

	return &newReport, nil
}

func truncate(str string, maxLen int) string {
	if len(str) > maxLen {
		return str[:maxLen-1]
	}
	return str
}
