FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM alpine:latest

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-8.6.1}

RUN wget https://github.com/zricethezav/gitleaks/releases/download/v${SCANNER_VERSION}/gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz  && \
    tar -xf gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz -C /usr/local/bin/ && \
    chmod a+x /usr/local/bin/gitleaks && \
    apk add --no-cache git

COPY --from=build --chown=root:root /go/src/app/analyzer /
COPY /gitleaks.toml /gitleaks.toml

ENTRYPOINT []
CMD ["/analyzer", "run"]
