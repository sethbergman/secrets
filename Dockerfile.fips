FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.17 AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM registry.access.redhat.com/ubi8/s2i-core
USER root

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-8.6.1}

ADD https://github.com/zricethezav/gitleaks/releases/download/v${SCANNER_VERSION}/gitleaks_${SCANNER_VERSION}_linux_x64.tar.gz  /tmp/gitleaks.tar.gz
RUN tar -xf /tmp/gitleaks.tar.gz -C /usr/local/bin/ && \
    chmod a+x /usr/local/bin/gitleaks && \
    rm -rf /tmp/gitleaks.tar.gz

RUN yum install git -y

# give write access to CA certificates (OpenShift)
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
	chmod g+w /etc/ssl/certs/ca-certificates.crt

RUN useradd --create-home gitlab -g root

COPY --from=build /go/src/app/analyzer /
COPY /gitleaks.toml /gitleaks.toml
USER gitlab

ENTRYPOINT []
CMD ["/analyzer", "run"]
