package main

import (
	"crypto/sha256"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const defaultSha = "0000000"

// Secret represents a gitleaks leak
type Secret struct {
	StartLine   int    `json:"StartLine"`
	Context     string `json:"Context"`
	Secret      string `json:"Secret"`
	Rule        string `json:"RuleID"`
	Description string `json:"Description"`
	Commit      string `json:"Commit"`
	File        string `json:"File"`
	Message     string `json:"Message"`
	Author      string `json:"Author"`
	Date        string `json:"Date"`
}

func (s *Secret) identifiers() []report.Identifier {
	ruleID := s.Rule
	if ruleID == "" {
		ruleID = s.Description
	}
	return []report.Identifier{
		{
			Type:  report.IdentifierType("gitleaks_rule_id"),
			Name:  fmt.Sprintf("Gitleaks rule ID %s", ruleID),
			Value: ruleID,
		},
	}
}

func (s *Secret) location() report.Location {
	commit := report.Commit{
		Author:  s.Author,
		Date:    s.Date,
		Message: s.Message,
		Sha:     s.Commit,
	}
	if commit.Sha == "" {
		commit.Sha = defaultSha
	}
	return report.Location{
		File:      s.File,
		LineStart: s.StartLine,
		LineEnd:   s.StartLine,
		Commit:    &commit,
	}
}

func (s *Secret) fingerprint() string {
	// Cleanup the source code extract from the report.
	sourceCode := strings.TrimSpace(s.Secret)
	// create code fingerprint using SHA256.
	h := sha256.New()
	if _, err := h.Write([]byte(sourceCode)); err != nil {
		log.Warnf("fingerprint err: %s", err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}

func (s *Secret) compareKey() string {
	rule := s.Rule
	if rule == "" {
		rule = s.Description
	}
	fingerprint := s.fingerprint()
	if fingerprint != "" {
		return strings.Join([]string{s.File, fingerprint, rule}, ":")
	}
	return strings.Join([]string{s.File, rule}, ":")
}

func (s *Secret) message() string {
	return fmt.Sprintf("%s detected; please remove and revoke it if this is a leak.", s.Description)
}

func (s *Secret) description() string {
	if s.Commit != "" {
		return fmt.Sprintf("Historic %s secret has been found in commit %s.", s.Description, s.Commit)
	}
	return s.Description
}
