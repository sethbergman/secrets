# Secrets analyzer changelog

## v4.0.3
- Default to scan only one commit if `git log` range returns an err (!159)

## v4.0.2
- Log warning when git fetch/log errors are encountered (!158)

## v4.0.1
- Log debug information before returning (!157)

## v4.0.0
- Remove `SECRET_DETECTION_COMMIT*` options in favor of `SECRET_DETECTION_LOG_OPTIONS` (!156)
- Remove `SECRET_DETECTION_ENTROPY` (!156)
- Fix `SECRET_DETECTION_HISTORIC_SCAN` (!156)

## v3.27.1
- Add Yandex Cloud tokens detection rules (!142)

## v3.27.0
- Update gitleaks dependency to v8.6.1 (!154)
  - Normalize keyword check (#830)
  - Pre-regex-check keyword string compare (#825)
- Add custom CA suppport for FIPS docker image

## v3.26.1
- Add Dockerfile.fips for FIPS releases (!151)

## v3.26.0
- Update gitleaks dependency to v8.5.3 (!150)
    - skip content checks for path only rules
    - skip binary files with --no-git (#810)
    - Allow tag (#809)
    - Refactor detect, add entropy to all findings (#804)

## v3.25.5
- Tune password-in-url rule to reduce false positives (!138)

## v3.25.4
- Allow merge commits (!147)

## v3.25.3
- Fix invalid commit range error from not being caught (!146)

## v3.25.2
- Add missing character to GitLab PAT detection rule (!144)

## v3.25.1
- Add GitLab Runner Registration Token detection rule (!143)

## v3.25.0
- Update ruleset, report, and command modules to support ruleset overrides (!141)

## v3.24.8
- Disable Hachicorp Vault service token detection (!137)

## v3.24.7
- Check for no changes (!134)

## v3.24.6
- Pass `SECURE_LOG_LEVEL` value down to gitleaks' log level (!135)

## v3.24.5
- Restrict Hashicorp Vault service token detection regex (!136)

## v3.24.4
- Add Hashicorp Vault token detection rules (!133)

## v3.24.3
- Tune password-in-url rule to reduce false positives (!132)

## v3.24.2
- Log gitleaks output async (!130)
- Remove `GIT_DEPTH` dependency
- Update gitleaks dependency to v8.2.7 (!130)
    - remove godoc text filtering (#763)
    - limit number of goroutines for historic scanning (#761)
    - always write sarif results if report-path is set
    - limit goroutines on file scanning (#759)

## v3.24.1
- Update gitleaks dependency to v8.2.3 (!129)

## v3.24.0
- Update gitleaks dependency to v8.2.1 (!127)

## v3.23.1
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!128)

## v3.23.0
- Add: New secret detection rules (!126)

## v3.22.1
- chore: Update go to v1.17 (!125)

## v3.22.0
- Add [Age](https://age-encryption.org) secret key detection rule (!122)

## v3.21.0
- Add GitLab Personal Access Token regexes (!123)

## v3.20.1
- Resolve vulnerabilities (!120)

## v3.20.0
- Update gitleaks to `v7.5.0` (!112)

## v3.19.0
- Update gitleaks to `v7.4.0` (!109)
  - [`v7.4.0` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.4.0)
- Add the new [github token format](https://github.blog/changelog/2021-03-31-authentication-token-format-updates-are-generally-available/) rules

## v3.18.1
- Add `revocation_token` to PyPI rule (!107)

## v3.18.0
- Add PyPI token regexes (!104 @ewjoachim)

## v3.17.0
- Update report dependency in order to use the report schema version 14.0.0 (!53)

## v3.16.0
- Update gitleaks to `v7.3.0` (!101)
  - [`v7.3.0` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.3.0)

## v3.15.2
- Update json schema from 3.0.0 to 13.1.0 (!100)
- Add default SHA for `--no-git` scans (!100)

## v3.15.1
- Fix AWS token mismatch due to change in identifier (!98)

## v3.15.0
- Refactor the Secrets analyze to fit the Secure common interface. (!92)
- Update gitleaks to `v7.2.2` (!92)
  - [`v7.2.2` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.2.2)

## v3.14.0
- Update gitleaks to `v7.2.1` (!91)
  - [`v7.2.1` gitleaks release post](https://github.com/zricethezav/gitleaks/releases/tag/v7.2.1)

## v3.13.2
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!90)

## v3.13.1
- Add Shopify token regexes (!89 @JackMc)
- Fix GitHub capitalization in gitleaks.toml (!88 @bbodenmiller)

## v3.13.0
- Upgrade common to v2.22.0 (!87)
- Update urfave/cli to v2.3.0 (!87)

## v3.12.0
- Add disablement of rulesets (!86)

## v3.11.1
- Add invalid line number warning message to the vulnerability description (!84)
- Change invalid line "-1" in vulnerability location to default to "1" (!84)

## v3.11.0
- Add social security number regex to gitleaks toml (!83)

## v3.10.2
- Bump gitleaks to v6.2.0 (!82)

## v3.10.1
- Add `vulnerability.raw_source_code_extract` containing leaked token (!79)

## v3.10.0
- Add custom rulesets (!80)

## v3.9.3
- Update golang dependencies (!75)

## v3.9.2
- Fix bug when `GIT_DEPTH` exceeds listed commits during non-default-branch scans (!72)

## v3.9.1
- Update Dockerfile and golang dependencies to latest versions (!71)

## v3.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!70)

## v3.8.0
- Bump gitleaks to v6.1.2 (!69)
- Add `SECRET_DETECTION_COMMITS` and `SECRET_DETECTION_COMMITS_FILE` options

## v3.7.2
- Upgrade go to version 1.15 (!68)

## v3.7.1
- Bump secrets module from v2 to v3 (!65)

## v3.7.0
- Replace `SAST_EXCLUDED_PATHS` with `SECRET_DETECTION_EXCLUDED_PATHS` (!64)

## v3.6.0
- Use scanner instead of analyzer in `scan.scanner` object (!62)

## v3.5.0
- Bump gitleaks to v5.0.1 (!60)

## v3.4.1
- Increase possible password length in URL regex (!61)

## v3.4.0
- Add scan object to report (!56)

## v3.3.1
- Fix `slack token` reporting (!57)

## v3.3.0
- Remove Trufflehog dependency from the analyzer (!52)

## v3.2.0
- Switch to the MIT Expat license (!54)

## v3.1.0
- Update logging to be standardized across analyzers (!50)

## v3.0.5
- Fixes `SECRET_DETECTION_HISTORIC_SCAN` bug when scanning non-existent files on the default branch (!48)

## v3.0.4
- Bump gitleaks to v4.3.1 (!49)

## v3.0.3
- Change env var prefix from `SAST_GITLEAKS_` to `SECRET_DETECTION_`

## v3.0.2
- Ignore QA test so we can release v3.x. v3.0.1 did not include a changelog entry so we need to bump once more.

## v3.0.1
- Ignore QA test so we can release v3.x

## v3.0.0
- Add standalone secret detection support (!42)

## v2.8.0
- Bump gitleaks and trufflehog (!38)

## v2.7.0
- Adds the `SAST_EXCLUDED_PATHS` env flag (!37)

## v2.6.0
- Add `id` field to vulnerabilities in JSON report (!36)

## v2.5.0
- Add commit range scanning

## v2.4.0
- Add historic scanning (!27)

## v2.3.0
- Add support for custom CA certs (!30)

## v2.2.3
- Update Gitleaks from 1.24.0 to 3.3.0

## v2.2.2
- Add check for env vars in password-in-url vulnerabilities

## v2.2.1
- Update common to v2.1.6

## v2.2.0
- Remove diffence (https://gitlab.com/gitlab-org/security-products/analyzers/secrets/merge_requests/13)

## v2.1.1
- Fix typos in reported messages

## v2.1.0
- Add support for generic api keys (https://gitlab.com/gitlab-org/gitlab/issues/10594)

## v2.0.5
- Set default severity value to Critical

## v2.0.4
- Fix: Set default Gitleaks entropy level to maximum to suppress false positives

## v2.0.3
- Fix: Update gitleaks file reader to better handle large files, fixes buffer overflow
- Fix: Update incorrect gitleaks rule RKCS8 to PKCS8

## v2.0.2
- Add gitleaks config, exclude svg analysis

## v2.0.1
- Fix gitleaks integration: don't parse output when there are no leaks

## v2.0.0
- Initial release
