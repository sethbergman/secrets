module gitlab.com/gitlab-org/security-products/analyzers/secrets/v3

require (
	github.com/pelletier/go-toml v1.9.4
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.6.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.9.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
)

go 1.15
